import React, { useState } from "react";
import { useAsyncDebounce } from "react-table";

const GlobaFilter = ({ filter, setFilter }) => {
  const [value, setValue] = useState(filter);

  // set filter value to 'value' or 'undefined' but only after timestamp (2 second), since the last onChange event fired.
  const onChange = useAsyncDebounce((value) => {
    setFilter(value || undefined);
  }, 2000);

  return (
    <span>
      Search:{" "}
      <input
        type="text"
        value={value || ""}
        onChange={(e) => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
      />
    </span>
  );
};

export default GlobaFilter;
