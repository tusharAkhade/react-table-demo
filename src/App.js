import "./App.css";
import BasicTable from "./components/BasicTable";
import ColumnHiding from "./components/ColumnHiding";
import ColumnOrder from "./components/ColumnOrder";
import FilteringTable from "./components/Filtering/FilteringTable";
import BasicTableWithFooter from "./components/FooterTable";
import GroupedColumnTable from "./components/GroupColumns";
import PaginationTable from "./components/Pagination/PaginationTable";
import RowSelection from "./components/RowSelection/RowSelection";
import SortingTable from "./components/SortingTable";

function App() {
  return (
    <div className="App">
      {/* <BasicTable /> */}
      {/* <BasicTableWithFooter /> */}
      {/* <GroupedColumnTable /> */}
      {/* <SortingTable /> */}
      {/* <FilteringTable /> */}
      {/* <PaginationTable /> */}
      {/* <RowSelection /> */}
      {/* <ColumnOrder /> */}
      <ColumnHiding />
    </div>
  );
}

export default App;
